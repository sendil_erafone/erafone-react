import React from 'react';
import Accordion from '@mui/material/Accordion';
import AccordionSummary from '@mui/material/AccordionSummary';
import AccordionDetails from '@mui/material/AccordionDetails';
import Typography from '@mui/material/Typography';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import PropTypes from 'prop-types';
//import Button from '@mui/material/Button';
import { styled } from '@mui/material/styles';
import Dialog from '@mui/material/Dialog';
import DialogTitle from '@mui/material/DialogTitle';
import DialogContent from '@mui/material/DialogContent';
//import DialogActions from '@mui/material/DialogActions';
import IconButton from '@mui/material/IconButton';
import CloseIcon from '@mui/icons-material/Close';
import Box from '@mui/material/Box';
//import Paper from '@mui/material/Paper';
import Grid from '@mui/material/Grid';
import TwitterIcon from '@mui/icons-material/Twitter';
import FacebookIcon from '@mui/icons-material/Facebook';
import WhatsAppIcon from '@mui/icons-material/WhatsApp';
import LinkIcon from '@mui/icons-material/Link';
import ReferralQr from './ReferralQr';

const BootstrapDialog = styled(Dialog)(({ theme }) => ({
    '& .MuiDialogContent-root': {
        padding: theme.spacing(2),
    },
    '& .MuiDialogActions-root': {
        padding: theme.spacing(1),
    },
}));

const BootstrapDialogTitle = (props) => {
    const { children, onClose, ...other } = props;

    return (
        <DialogTitle sx={{ m: 0, p: 2 }} {...other}>
            {children}
            {onClose ? (
                <IconButton
                    aria-label="close"
                    onClick={onClose}
                    sx={{
                        position: 'absolute',
                        right: 8,
                        top: 8,
                        color: (theme) => theme.palette.grey[500],
                    }}
                >
                    <CloseIcon />
                </IconButton>
            ) : null}
        </DialogTitle>
    );
};

BootstrapDialogTitle.propTypes = {
    children: PropTypes.node,
    onClose: PropTypes.func.isRequired,
};


function ReferralCode() {
    const refercode=window.sessionStorage.getItem("referal_code");
    const [open, setOpen] = React.useState(false);
    
    const handleClickOpen = () => {
        setOpen(true);
    };
    const handleClose = () => {
        setOpen(false);
    };

    return (
        <div className=''>

            <div className='container'>
             
            </div>

            <div id="demo" className="carousel slide" data-ride="carousel">


                <ul className="carousel-indicators">
                    <li data-target="#demo" data-slide-to="0" className="active"></li>
                    <li data-target="#demo" data-slide-to="1"></li>
                    <li data-target="#demo" data-slide-to="2"></li>
                </ul>


                <div className="carousel-inner">
                    <div className="carousel-item active">
                        <img src={require('./Images/era2.jpg')} alt="Los Angeles" width="1100" height="300" />
                    </div>
                    <div className="carousel-item">
                        <img src={require('./Images/era2.jpg')} alt="Chicago" width="1100" height="300" />
                    </div>
                    <div className="carousel-item">
                        <img src={require('./Images/era2.jpg')} alt="New York" width="1100" height="300" />
                    </div>
                </div>


                <a className="carousel-control-prev" href="#demo" data-slide="prev">
                    <span className="carousel-control-prev-icon"></span>
                </a>
                <a className="carousel-control-next" href="#demo" data-slide="next">
                    <span className="carousel-control-next-icon"></span>
                </a>

            </div>
            <br />


            <div className='container'>
                <div className='row'>
                    <div className='col-xs-12 col-sm-12 col-lg-2 form-group'></div>
                    <div className='col-xs-12 col-sm-12 col-lg-8 form-group text-center'>
                        <h2 className='text-center'>Ajak Teman atau Keluargamu dan Dapatkan Hadiah Eraclub Gift Card atau poin</h2>
                    </div>
                    <div className='col-xs-12 col-sm-12 col-lg-2 form-group'></div>
                </div>

                <h5 className='text-center g-clr'>Lorem Ipsum</h5>
                <br />
                <Grid><p className='text-center'><ReferralQr/></p></Grid>

                <div className='row'>
                    <div className='col-xs-12 col-sm-12 col-lg-4 form-group'></div>
                    <div className='col-xs-12 col-sm-12 col-lg-4 form-group text-center'>
                        <div>
                            <h6 className=''>Kode Rujukan</h6>
                            <h4 className=''>
                                {refercode} &nbsp;
                                <i class="fa fa-clone" aria-hidden="true"></i>
                            </h4>
                        </div>
                    </div>
                </div>

                <div className='row'>
                    <div className='col-xs-12 col-sm-12 col-lg-4 form-group'></div>
                    <div className='col-xs-12 col-sm-12 col-lg-4 form-group text-center'>
                        <button type="submit" class="btn btn-danger" onClick={handleClickOpen}>
                            <i class="fa fa-share-alt" aria-hidden="true"></i>
                            &nbsp; Ajak Teman atau Keluarga
                        </button>
                    </div>
                    <div className='col-xs-12 col-sm-12 col-lg-4 form-group'></div>
                </div>
                <br />

                <BootstrapDialog
                    onClose={handleClose}
                    aria-labelledby="customized-dialog-title"
                    open={open}
                >
                    <BootstrapDialogTitle id="customized-dialog-title" onClose={handleClose}>
                        Invite Links
                    </BootstrapDialogTitle>
                    <DialogContent dividers>

                        <Box sx={{ flexGrow: 1 }}>
                            <Grid container spacing={2}>
                                <Grid item xs={12}>
                                    <TwitterIcon />&nbsp;Twitter
                                </Grid>
                                <Grid item xs={12}>
                                    <FacebookIcon />&nbsp;Facebook
                                </Grid>
                                <Grid item xs={12}>
                                    <WhatsAppIcon />&nbsp;WhatsApp
                                </Grid>
                                <Grid item xs={12}>
                                    < LinkIcon />&nbsp;Salin Link
                                </Grid>
                            </Grid>
                        </Box>

                    </DialogContent>
                    {/* <DialogActions>
                        <Button autoFocus onClick={handleClose}>
                            Save changes
                        </Button>
                    </DialogActions> */}
                </BootstrapDialog>


                <Accordion>
                    <AccordionSummary
                        expandIcon={<ExpandMoreIcon />}
                        aria-controls="panel1a-content"
                        id="panel1a-header"
                        className='btn-danger'
                    >
                        <Typography>Cara Mendapatkan Komisi</Typography>
                    </AccordionSummary>
                    <AccordionDetails>
                        <Typography>Lorem ipsum</Typography>
                        <Typography>Lorem ipsum</Typography>
                        <Typography>Lorem ipsum</Typography>
                    </AccordionDetails>
                </Accordion>
                <br />

                <Accordion>
                    <AccordionSummary
                        expandIcon={<ExpandMoreIcon />}
                        aria-controls="panel2a-content"
                        id="panel2a-header"
                        className='btn-danger'
                    >
                        <Typography>Syarat dan Ketentuan</Typography>
                    </AccordionSummary>
                    <AccordionDetails>
                    <Typography>Lorem ipsum</Typography>
                        <Typography>Lorem ipsum</Typography>
                        <Typography>Lorem ipsum</Typography>
                    </AccordionDetails>
                </Accordion>
            </div>
            <br />
            <h6 className='text-center g-clr'>COPYRIGHT @ 2022 ERAJAYA GROUP ALL RIGHTS RESERVED</h6>
        </div>

    )
}

export default ReferralCode;