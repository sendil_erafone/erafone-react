import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import './Style.css';
//import App from './App';
//import reportWebVitals from './reportWebVitals';
import { BrowserRouter as Router, Routes, Route, NavLink } from 'react-router-dom';
import Login from './Login';
import Forgot from './Forgot';
import Home from './Home'
import ReferralCode from './ReferralCode';
import UserRegist from './UserRegist';
import UserRegistNo from './UserRegistNo';
import UserRegistYes from './UserRegistYes';
import Commission from './Commission';
import ChkTable from './ChkTable';
import CBox2 from './CBox2';
import CBoxr from './CBoxr';
import ReferralQr from './ReferralQr';


const routing = (
  <Router>
  
    <nav className="navbar navbar-expand-sm bg-danger navbar-dark">
      <div className="container">
        <ul className="navbar-nav">
          <li className="nav-item"><NavLink className="nav-link" to="/Info">Franchisee info</NavLink></li>
          <li className="nav-item"><NavLink className="nav-link" to="/Advantage">Franchisee Advantage</NavLink></li>
          <li className="nav-item"><NavLink className="nav-link" to="/Investment">Investment</NavLink></li>
          <li className="nav-item"><NavLink className="nav-link" to="/UserRegist">Franchisee Registration</NavLink></li>
          <li className="nav-item"><NavLink className="nav-link" to="/Contact">Contact Us</NavLink></li>
          <li className="nav-item"><NavLink className="nav-link" to="/Franchisee">Franchisee Report</NavLink></li>
          <li className="nav-item"><NavLink className="nav-link active" to="/Login">Login</NavLink></li>
        </ul>

        <div className='text-right'>
          <img src={require('./Images/erafone.jpg')} alt="Erafone"/>
        </div>
      </div>
    </nav>
    

    <Routes>
      <Route exact path="/" element={<Login />} />
      <Route exact path="/Login" element={<Login />} />      
      <Route exact path="/UserRegist" element={<UserRegist />} />
      <Route exact path="/UserRegistNo" element={<UserRegistNo />} />
      <Route exact path="/UserRegistYes" element={<UserRegistYes />} />      
      <Route exact path="/Forgot" element={<Forgot />} />
      <Route exact path="/Home" element={<Home />} />
      <Route exact path="/ReferralCode" element={<ReferralCode />} />
      <Route exact path="/Commission" element={<Commission />} />
      <Route exact path="/ChkTable" element={<ChkTable />} />
      <Route exact path="/CBox2" element={<CBox2 />} />
      <Route exact path="/CBoxr" element={<CBoxr />} />  
      <Route exact path="/ReferralQr" element={<ReferralQr />} />                
    </Routes>

  </Router>
);

ReactDOM.render(routing, document.getElementById('root'));


// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
//reportWebVitals();
