import React from "react";
import { styled } from '@mui/material/styles';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import CardActions from '@mui/material/CardActions';
import Collapse from '@mui/material/Collapse';
import IconButton from '@mui/material/IconButton';
import Typography from '@mui/material/Typography';
//import { red } from '@mui/material/colors';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import Grid from '@mui/material/Grid';

const ExpandMore = styled((props) => {
    const { expand, ...other } = props;
    return <IconButton {...other} />;
})(({ theme, expand }) => ({
    transform: !expand ? 'rotate(0deg)' : 'rotate(180deg)',
    marginLeft: 'auto',
    transition: theme.transitions.create('transform', {
        duration: theme.transitions.duration.shortest,
    }),
}));

function Expand() {

    const [expanded, setExpanded] = React.useState(false);

    const handleExpandClick = () => {
        setExpanded(!expanded);
    };

    return (
        <div className="">
            <IconButton aria-label="#">
                <label className='btn-rev1'>Lihat Detail</label>
            </IconButton>
            <ExpandMore
                expand={expanded}
                onClick={handleExpandClick}
                aria-expanded={expanded}
                aria-label="show more"
            >
                <ExpandMoreIcon />
            </ExpandMore>


            <Collapse in={expanded} timeout="auto" unmountOnExit>
                <CardContent>
                    <Grid container spacing={1}>
                        <Grid item xs={12} xm={6} md={6}>
                            <Card>
                                <CardContent>
                                    <img src={require('./Images/gifticon.jpg')} /><br /><br />
                                    <Typography sx={{ mb: 1.5 }} color="text.secondary">Total Gift Card Digital</Typography>
                                    <Typography><h4>Rp4.000.000</h4></Typography>
                                    <Typography sx={{ mb: 1.5 }} color="text.secondary">/Rp5.000.000</Typography>
                                </CardContent>
                            </Card>
                        </Grid>
                        <Grid item xs={12} xm={6} md={6}>
                            <Card>
                                <CardContent>
                                    <img src={require('./Images/eraclubicon.jpg')} /><br /><br />
                                    <Typography sx={{ mb: 1.5 }} color="text.secondary">Total Eraclub Point</Typography>
                                    <Typography><h4>Rp4.000.000</h4></Typography>
                                    <Typography sx={{ mb: 1.5 }} color="text.secondary">/Rp5.000.000</Typography>
                                </CardContent>
                            </Card>
                        </Grid>
                    </Grid>
                </CardContent>
            </Collapse>

        </div>
    )
}

export default Expand;