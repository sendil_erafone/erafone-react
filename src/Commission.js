import React from 'react';
import Grid from '@mui/material/Grid';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import Button from '@mui/material/Button';
import ChkTable from './ChkTable';
import Typography from '@mui/material/Typography';
import Expand from './Expand';
import PropTypes from 'prop-types';
import { styled } from '@mui/material/styles';
import Dialog from '@mui/material/Dialog';
import DialogTitle from '@mui/material/DialogTitle';
import DialogContent from '@mui/material/DialogContent';
import DialogActions from '@mui/material/DialogActions';
import IconButton from '@mui/material/IconButton';
import CloseIcon from '@mui/icons-material/Close';
import Collapse from '@mui/material/Collapse';
import FormControl from '@mui/material/FormControl';
import FormControlLabel from '@mui/material/FormControlLabel';
import FormLabel from '@mui/material/FormLabel';
import Radio from '@mui/material/Radio';
import RadioGroup from '@mui/material/RadioGroup';

const ExpandIn = styled((props) => {
    const { expand, ...other } = props;
    return <Card {...other} />;
})(({ theme, expand }) => ({
    // transform: !expand ? 'rotate(0deg)' : 'rotate(180deg)',
    marginLeft: 'auto',
    transition: theme.transitions.create('transform', {
        duration: theme.transitions.duration.shortest,
    }),
}));

const ExpandOut = styled((props) => {
    const { expand, ...other } = props;
    return <Card {...other} />;
})(({ theme, expand }) => ({
    // transform: !expand ? 'rotate(0deg)' : 'rotate(180deg)',
    marginLeft: 'auto',
    transition: theme.transitions.create('transform', {
        duration: theme.transitions.duration.shortest,
    }),
}));


const BootstrapDialog = styled(Dialog)(({ theme }) => ({
    '& .MuiDialogContent-root': {
        padding: theme.spacing(2),
    },
    '& .MuiDialogActions-root': {
        padding: theme.spacing(1),
    },
}));

const BootstrapDialogTitle = (props) => {
    const { children, onClose, ...other } = props;

    return (
        <DialogTitle sx={{ m: 0, p: 2 }} {...other}>
            {children}
            {onClose ? (
                <IconButton
                    aria-label="close"
                    onClick={onClose}
                    sx={{
                        position: 'absolute',
                        right: 8,
                        top: 8,
                        color: (theme) => theme.palette.grey[500],
                    }}
                >
                    <CloseIcon />
                </IconButton>
            ) : null}
        </DialogTitle>
    );
};

BootstrapDialogTitle.propTypes = {
    children: PropTypes.node,
    onClose: PropTypes.func.isRequired,
};


function Commission() {

    const [expandedIn, setExpandedIn] = React.useState(false);

    const handleExpandInClick = () => {
        setExpandedIn(!expandedIn);
    };

    const [expandedOut, setExpandedOut] = React.useState(false);

    const handleExpandOutClick = () => {
        setExpandedOut(!expandedOut);
    };

    const [open, setOpen] = React.useState(false);

    const handleClickOpen = () => {
        setOpen(true);
    };
    const handleClose = () => {
        setOpen(false);
    };

    return (
        <div className='container'>
            <br /><br />

            <Grid container spacing={1}>
                <Grid item xs={12} sm={6} md={6}>
                    <h3 className=''>Performa Rujukan Anda</h3>
                </Grid>
                <Grid item xs={12} sm={6} md={6} className="text-right">
                    <Button type="submit" className="btn btn-danger btn-sub">
                        Lihat Kode Rujukan
                    </Button>
                </Grid>
            </Grid>
            <br />

            <Grid container spacing={1}>

                <Grid item xs={12} sm={6} md={6}>

                    <Card className="bg-bor">
                        <CardContent>
                            <Grid container spacing={1}>
                                <Grid item xs={12} sm={6} md={6}>
                                    <Typography sx={{ fontSize: 14 }} color="text.secondary" gutterBottom>
                                        Total Komisi
                                    </Typography>
                                    <Typography variant="h5" component="div">
                                        Rp 8.000.00
                                    </Typography>
                                    <Typography sx={{ mb: 1.5 }} color="text.secondary">
                                        /Rp10.000.000
                                    </Typography>
                                </Grid>
                                <Grid item xs={12} sm={6} md={6} className="text-right">
                                    <img src={require('./Images/rpicon.png')} />
                                </Grid>
                            </Grid>
                            <br />
                            <label className="lborder1" >Komisi yung akan dibayarkan:<b>Rp2.000.000</b></label>
                            <br />
                            <Expand />

                        </CardContent>
                        <CardActions>

                        </CardActions>

                    </Card>

                </Grid>

                <Grid item xs={12} sm={6} md={6}>

                    <Card className="bg-bor">
                        <CardContent>
                            <Grid container spacing={1}>
                                <Grid item xs={12} sm={6} md={6}>
                                    <Typography sx={{ fontSize: 14 }} color="text.secondary" gutterBottom>
                                        Rujukan yung Berhasil
                                    </Typography>
                                    <Typography variant="h5" component="div">
                                        5 Orang
                                    </Typography>
                                    <Typography sx={{ mb: 1.5 }} color="text.secondary">
                                        /10 Orang
                                    </Typography>
                                </Grid>
                                <Grid item xs={12} sm={6} md={6} className="text-right">
                                    <img src={require('./Images/usericon.png')} />
                                </Grid>
                            </Grid>
                            <br />
                            <label className="lborder" >50% Rujukan Anda Berhasil disetujui</label>

                        </CardContent>
                        <CardActions>

                        </CardActions>
                    </Card>

                </Grid>
            </Grid>
            <br />

            <h2 className=''>Status Rujukan Anda</h2>
            <br />

            <button type="submit" className="btn btn-danger btn-sub" onClick={handleClickOpen}>
                Pilip Tipe Komasi
            </button>
            <br /><br />

            <BootstrapDialog
                onClose={handleClose}
                aria-labelledby="customized-dialog-title"
                open={open}

            >
                <BootstrapDialogTitle id="customized-dialog-title"  onClose={handleClose}>
                    Pilip Tipe Komasi
                </BootstrapDialogTitle>

                <DialogContent dividers>

                    <Grid container spacing={1}>

                        <Grid item xs={12} xm={6} md={6}>
                            <ExpandIn
                                expand={expandedIn}
                                onClick={handleExpandInClick}
                                aria-expanded={expandedIn}
                                aria-label="show more"
                            >
                                <Card>
                                    <CardContent>
                                        <Grid container spacing={1}>
                                            <Grid item xs={12} xm={8} md={12} className="text-center">
                                                <img src={require('./Images/gifticon.jpg')} className='' /><br /><br />
                                                <h6 className=''>Gift Card Digital</h6>
                                            </Grid>
                                        </Grid>
                                    </CardContent>
                                </Card>
                            </ExpandIn>
                        </Grid>

                        <Grid item xs={12} xm={6} md={6}>
                            <ExpandOut
                                expand={expandedOut}
                                onClick={handleExpandOutClick}
                                aria-expanded={expandedOut}
                                aria-label="show more"
                            >
                                <Card>
                                    <CardContent>
                                        <Grid container spacing={1}>
                                            <Grid item xs={12} xm={6} md={12} className="text-center">
                                                <img src={require('./Images/eraclubicon.jpg')} /><br /><br />
                                            </Grid>
                                        </Grid>
                                        <h6 className='text-center'>Eraclub Point</h6>
                                    </CardContent>
                                </Card>
                            </ExpandOut>
                        </Grid>

                    </Grid>

                    <Collapse in={expandedIn} timeout="auto" unmountOnExit>
                        <br />
                        <Card>
                            <CardContent>
                                <h6 className="">Pilih Salah Santu Jenis Gift Card Digital Yang Dinijikran</h6>
                                <FormControl>
                                    <FormLabel id="demo-radio-buttons-group-label"></FormLabel>
                                    <RadioGroup
                                        aria-labelledby="demo-radio-buttons-group-label"
                                        defaultValue=""
                                        name="radio-buttons-group"
                                    >
                                        <FormControlLabel value="female" control={<Radio />} label="100 Gift Card dengan masing-masing senillai Rp50.000" />
                                        <FormControlLabel value="female" control={<Radio />} label="50 Gift Card dengan masing-masing senillai Rp50.000" />
                                        <FormControlLabel value="female" control={<Radio />} label="100 Gift Card dengan masing-masing senillai Rp50.000" />
                                        <FormControlLabel value="female" control={<Radio />} label="100 Gift Card dengan masing-masing senillai Rp50.000" />
                                        <FormControlLabel value="female" control={<Radio />} label="100 Gift Card dengan masing-masing senillai Rp50.000" />
                                    </RadioGroup>
                                </FormControl>
                            </CardContent>
                        </Card>
                    </Collapse>


                    <Collapse in={expandedOut} timeout="auto" unmountOnExit>
                        <br />
                        <Card>
                            <CardContent>
                            <label className="lborder" >50% Rujukan Anda Berhasil disetujui</label>
                            </CardContent>
                        </Card>
                    </Collapse>




                </DialogContent>

                <DialogActions>
                    <Button className="btn btn-danger btn-sub">
                        Kempali
                    </Button>
                    <Button className="btn btn-danger btn-pop" disable>
                        Simpan
                    </Button>
                </DialogActions>

            </BootstrapDialog >



            <ChkTable />


        </div >
    )
}

export default Commission;