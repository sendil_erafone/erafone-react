import * as React from 'react';
import Box from '@mui/material/Box';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';

const bull = (
  <Box
    component="span"
    sx={{ display: 'inline-block', mx: '2px', transform: 'scale(0.8)' }}
  >
    •
  </Box>
);

export default function CBoxr() {
  return (
    <Card sx={{ minWidth: 350 }}>
      <CardContent>
        <Typography sx={{ fontSize: 14 }} color="text.secondary" gutterBottom>
          Rujukan yung Berhasil 
        </Typography>
        <Typography variant="h5" component="div">
          5 Orang
        </Typography>
        <Typography sx={{ mb: 1.5 }} color="text.secondary">
          /10 Orang
        </Typography>       
      </CardContent>
      <CardActions>        
        <label className="lborder" >50% Rujukan Anda Berhasil disetujui</label>
      </CardActions>
    </Card>
  );
}