import React from 'react';
import { NavLink } from 'react-router-dom';


function Registration() {

    return (
        <div className='container'>
            <br />
            <div className="card">
                <div className="card-body">
                    <h3 className='text-center'>FILL IN THE FORM BELOW TO BECOME PART OF US</h3>
                    <br />
                    <h4 className='text-center'>APPLICANT'S PERSONAL DATA</h4>
                    <hr />

                    <form className='container'>
                        <div className='row'>
                            <div className="col-xs-12 col-sm-12 col-lg-4 form-group">
                                <label>Full Name *</label>
                            </div>
                            <div className="col-xs-12 col-sm-12 col-lg-8 form-group">
                                <input type="text" placeholder='FullName' name="fullname" className='form-control' />
                            </div>
                        </div>

                        <div className='row'>
                            <div className="col-xs-6 col-sm-6 col-lg-4 form-group">
                                <label>Gender *</label>
                            </div>
                            <div className="col-xs-1 col-sm-1 col-lg-8 form-group">
                                <div class="form-check-inline">
                                    <label class="form-check-label">
                                        <input type="radio" name="man" class='form-check-input' />Man
                                    </label>
                                </div>
                                <div class="form-check-inline">
                                    <label class="form-check-label">
                                        <input type="radio" name="woman" className='form-check-input' />Woman
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div className='row'>
                            <div className="col-xs-12 col-sm-12 col-lg-4 form-group">
                                <label>Province *</label>
                            </div>
                            <div className="col-xs-12 col-sm-12 col-lg-8 form-group">
                                <input type="text" placeholder='Province' name="province" className='form-control' />
                            </div>
                        </div>

                        <div className='row'>
                            <div className="col-xs-12 col-sm-12 col-lg-4 form-group">
                                <label>Municipality / District *</label>
                            </div>
                            <div className="col-xs-12 col-sm-12 col-lg-8 form-group">
                                <input type="text" placeholder='Municipality / District' name="municipal" className='form-control' />
                            </div>
                        </div>

                        <div className='row'>
                            <div className="col-xs-12 col-sm-12 col-lg-4 form-group">
                                <label>District *</label>
                            </div>
                            <div className="col-xs-12 col-sm-12 col-lg-8 form-group">
                                <input type="text" placeholder='District' name="district" className='form-control' />
                            </div>
                        </div>

                        <div className='row'>
                            <div className="col-xs-12 col-sm-12 col-lg-4 form-group">
                                <label>Village *</label>
                            </div>
                            <div className="col-xs-12 col-sm-12 col-lg-8 form-group">
                                <input type="text" placeholder='Village' name="Village" className='form-control' />
                            </div>
                        </div>

                        <div className='row'>
                            <div className="col-xs-12 col-sm-12 col-lg-4 form-group">
                                <label>Home Address *</label>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-lg-4 form-group">
                                <textarea class="form-control" rows="5" id="comment"></textarea>
                            </div>

                        </div>

                        <div className='row'>
                            <div className="col-xs-12 col-sm-12 col-lg-4 form-group">
                                <label>RT / RW</label>
                            </div>
                            <div className="col-xs-12 col-sm-12 col-lg-8 form-group">
                                <input type="text" placeholder='RT / RW' name="rt" className='form-control' />
                            </div>
                        </div>

                        <div className='row'>
                            <div className="col-xs-12 col-sm-12 col-lg-4 form-group">
                                <label>Postal Code *</label>
                            </div>
                            <div className="col-xs-12 col-sm-12 col-lg-8 form-group">
                                <input type="text" placeholder='Postal Code' name="postalcode" className='form-control' />
                            </div>
                        </div>

                        <div className='row'>
                            <div className="col-xs-12 col-sm-12 col-lg-4 form-group">
                                <label>Phone / Fax</label>
                            </div>
                            <div className="col-xs-12 col-sm-12 col-lg-8 form-group">
                                <input type="text" placeholder='Phone / Fax' name="phone" className='form-control' />
                            </div>
                        </div>

                        <div className='row'>
                            <div className="col-xs-12 col-sm-12 col-lg-4 form-group">
                                <label>Cellphone *</label>
                            </div>
                            <div className="col-xs-12 col-sm-12 col-lg-8 form-group">
                                <form>
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">62</span>
                                        </div>
                                        <input type="text" placeholder="XXXXXXXXX" class='form-control' />
                                    </div>


                                </form>
                            </div>
                        </div>


                        <div className='row'>
                            <div className="col-xs-12 col-sm-12 col-lg-4 form-group">
                                <label>Email *</label>
                            </div>
                            <div className="col-xs-12 col-sm-12 col-lg-8 form-group">
                                <input type="text" placeholder='Email' name="email" className='form-control' />
                            </div>
                        </div>

                        <div className='row'>
                            <div className="col-xs-12 col-sm-12 col-lg-4 form-group">
                                <label>Information Source *</label>
                            </div>
                            <div className="col-xs-12 col-sm-12 col-lg-8 form-group">
                                <label>Social Media / Online *</label>

                                <div class="form-check">
                                    <label class="form-check-label">
                                        <input type="checkbox" value="" className='form-check-input' />Website
                                    </label>
                                </div>
                                <div class="form-check">
                                    <label class="form-check-label">
                                        <input type="checkbox" value="" className='form-check-input' />Instagram
                                    </label>
                                </div>
                                <div class="form-check">
                                    <label class="form-check-label">
                                        <input type="checkbox" value="" className='form-check-input' />E-Mail
                                    </label>
                                </div>
                                <div className="form-check">
                                    <label >
                                        <input type="checkbox" value="" className='form-check-input' />Whatsapp
                                    </label>
                                </div>

                                <div className='row'>
                                    <div className="col-xs-12 col-sm-12 col-lg-2 form-group">
                                        <label>Others *</label>
                                    </div>
                                    <div className="col-xs-12 col-sm-12 col-lg-10 form-group">
                                        <input type="text" placeholder='Mention' name="Mention" className='form-control' />
                                    </div>
                                </div>

                                <label>Print Media *</label>

                                <div class="form-check">
                                    <label class="form-check-label">
                                        <input type="checkbox" value="" className='form-check-input' />Booklet
                                    </label>
                                </div>
                                <div class="form-check">
                                    <label class="form-check-label">
                                        <input type="checkbox" value="" className='form-check-input' />Magazine
                                    </label>
                                </div>
                                <div class="form-check">
                                    <label>
                                        <input type="checkbox" value="" className='form-check-input' />News Paper
                                    </label>
                                </div>

                                <div className='row'>
                                    <div className="col-xs-12 col-sm-12 col-lg-2 form-group">
                                        <label>Others *</label>
                                    </div>
                                    <div className="col-xs-12 col-sm-12 col-lg-10 form-group">
                                        <input type="text" placeholder='Mention' name="Mention" className='form-control' />
                                    </div>
                                </div>

                                <div class="form-check">
                                    <label class="form-check-label">
                                        <input type="checkbox" value="" className='form-check-input' />Exhibition Workshop
                                    </label>
                                </div>
                                <div class="form-check">
                                    <label class="form-check-label">
                                        <input type="checkbox" value="" className='form-check-input' />Erafone Franchise
                                    </label>
                                </div>
                                <div class="form-check">
                                    <label class="form-check-label">
                                        <input type="checkbox" value="" className='form-check-input' />Erajaya Employees
                                    </label>
                                </div>


                            </div>
                        </div>

                        <div className='row'>
                            <div className="col-xs-12 col-sm-12 col-lg-4 form-group">
                                <label>Do I have a place of business? *</label>
                            </div>
                            <div className="col-xs-12 col-sm-12 col-lg-8 form-group">
                                <div class="form-check-inline">
                                    <label class="form-check-label">
                                        <input type="radio" name="man" class='form-check-input' />Yes
                                    </label>
                                </div>
                                <div class="form-check-inline">
                                    <label class="form-check-label">
                                        <input type="radio" name="woman" className='form-check-input' />No
                                    </label>
                                </div>
                            </div>
                        </div>






                        <NavLink to="/Login" className="tn">
                            <div className='row'>
                                <div className="col-xs-12 col-sm-12 col-lg-12 form-group dg">
                                    <button type="button" className="btn btn-danger btn-sub">Submit</button>
                                </div>
                            </div>
                        </NavLink>

                    </form>


                </div>
            </div >

            <br />
        </div >
    )
}

export default Registration;