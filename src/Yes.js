import React from 'react';
import { NavLink } from 'react-router-dom';


function Yes() {

    return (
        <div className='container'>
            <br />
            <div className="card">
                <div className="card-body">
                   
                    <h4 className='text-center'>PROPOSED LOCATION DATA</h4>
                    <hr />

                    <form className='container'>
                        
                        <div className='row'>
                            <div className="col-xs-12 col-sm-12 col-lg-4 form-group">
                                <label>Province *</label>
                            </div>
                            <div className="col-xs-12 col-sm-12 col-lg-8 form-group">
                                <input type="text" placeholder='Province' name="province" className='form-control' />
                            </div>
                        </div>

                        <div className='row'>
                            <div className="col-xs-12 col-sm-12 col-lg-4 form-group">
                                <label>Municipality / District *</label>
                            </div>
                            <div className="col-xs-12 col-sm-12 col-lg-8 form-group">
                                <input type="text" placeholder='Municipality / District' name="municipal" className='form-control' />
                            </div>
                        </div>

                        <div className='row'>
                            <div className="col-xs-12 col-sm-12 col-lg-4 form-group">
                                <label>District *</label>
                            </div>
                            <div className="col-xs-12 col-sm-12 col-lg-8 form-group">
                                <input type="text" placeholder='District' name="district" className='form-control' />
                            </div>
                        </div>

                        <div className='row'>
                            <div className="col-xs-12 col-sm-12 col-lg-4 form-group">
                                <label>Village *</label>
                            </div>
                            <div className="col-xs-12 col-sm-12 col-lg-8 form-group">
                                <input type="text" placeholder='Village' name="Village" className='form-control' />
                            </div>
                        </div>

                        <div className='row'>
                            <div className="col-xs-12 col-sm-12 col-lg-4 form-group">
                                <label>Location Address *</label>
								</div>
                            <div class="form-group">
                                  <textarea class="form-control" rows="5" id="comment"></textarea>
                            </div>
          
                        </div>

                        <div className='row'>
                            <div className="col-xs-12 col-sm-12 col-lg-4 form-group">
                                <label>RT / RW</label>
                            </div>
							
							<div class="form-row">
									<div class="col">
									     <input type="text" placeholder='RT' name="rt" className='form-control' />
									</div>
									<div class="col">
									    <input type="text" placeholder='RW' name="rt" className='form-control' />
									</div>
								  </div>
						
                        </div>

                        <div className='row'>
                            <div className="col-xs-12 col-sm-12 col-lg-4 form-group">
                                <label>Postal Code *</label>
                            </div>
                            <div className="col-xs-12 col-sm-12 col-lg-8 form-group">
                                <input type="text" placeholder='Postal Code' name="postalcode" className='form-control' />
                            </div>
                        </div>

                         <div className='row'>
                            <div className="col-xs-12 col-sm-12 col-lg-4 form-group">
                                <label>PIC Phone / Owner *</label>
                            </div>
                            <div className="col-xs-12 col-sm-12 col-lg-8 form-group">
                           
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                <span class="input-group-text">62</span>
                                </div>
                                <input type="text" placeholder="XXXXXXXXX" class='form-control' />
                            </div>
  
                            </div>
                        </div>


                        <div className='row'>
                            <div className="col-xs-12 col-sm-12 col-lg-4 form-group">
                                <label>Location Length *</label>
                            </div>
                            <div className="col-xs-12 col-sm-12 col-lg-8 form-group">
                                <input type="text" placeholder='Location Length' name="location_length" className='form-control' />
                            </div>
                        </div>
						 <div className='row'>
                            <div className="col-xs-12 col-sm-12 col-lg-4 form-group">
                                <label>Location Width *</label>
                            </div>
                            <div className="col-xs-12 col-sm-12 col-lg-8 form-group">
                                <input type="text" placeholder='Location Width' name="location_width" className='form-control' />
                            </div>
                        </div>
						
						 <div className='row'>
                            <div className="col-xs-12 col-sm-12 col-lg-4 form-group">
                                <label>Location Area *</label>
                            </div>
                            <div className="col-xs-12 col-sm-12 col-lg-8 form-group">
                                <input type="text" placeholder='Location Area' name="location_area" className='form-control' />
                            </div>
                        </div>
						
                          <div className='row'>
                            <div className="col-xs-12 col-sm-12 col-lg-4 form-group">
                                <label>Number of Floors *</label>
                            </div>
                            <div className="col-xs-12 col-sm-12 col-lg-8 form-group">
                                <input type="text" placeholder='Location Area' name="location_area" className='form-control' />
                            </div>
                        </div>
						
                        <div className='row'>
                            
                            <div className="col-xs-12 col-sm-12 col-lg-8 form-group">
                            <label>Location Form *</label>

                            <div class="form-check">
                                <label class="form-check-label">
                                    <input type="checkbox" value ="" className='form-check-input' />Shop
                                </label>
                            </div>
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input type="checkbox" value="" className='form-check-input' />Kiosk/T.Business
                                </label>
                             </div>   
                             <div class="form-check">
                                <label class="form-check-label">
                                    <input type="checkbox" value ="" className='form-check-input' />Residence
                                </label>
                             </div>    
                             <div class="form-check">
                                <label class="form-check-label">   
                                    <input type="checkbox" value="" className='form-check-input' />Soil
                                    </label>
                               </div> 
                           </div>  
						</div>
							 
					     <div className='row'>
                            
                            <div className="col-xs-12 col-sm-12 col-lg-8 form-group">
                            <label>Location Status *</label>

                            <div class="form-check">
                                <label class="form-check-label">
                                    <input type="checkbox" value ="" className='form-check-input' />One's Own
                                </label>
                            </div>
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input type="checkbox" value="" className='form-check-input' />Family Owned
                                </label>
                             </div>   
                             <div class="form-check">
                                <label class="form-check-label">
                                    <input type="checkbox" value ="" className='form-check-input' />Company Owned
                                </label>
                             </div>    
                             <div class="form-check">
                                <label class="form-check-label">   
                                    <input type="checkbox" value="" className='form-check-input' />Rent
                                    </label>
                               </div> 
                           </div>  
						</div>

                         <div className='row'>
                            
                            <div className="col-xs-12 col-sm-12 col-lg-8 form-group">
                            <label>Location in Region *</label>

                            <div class="form-check">
                                <label class="form-check-label">
                                    <input type="checkbox" value ="" className='form-check-input' />Settlement
                                </label>
                            </div>
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input type="checkbox" value="" className='form-check-input' />Office/Apartment
                                </label>
                             </div>   
                             <div class="form-check">
                                <label class="form-check-label">
                                    <input type="checkbox" value ="" className='form-check-input' />Office/School
                                </label>
                             </div>    
                             <div class="form-check">
                                <label class="form-check-label">   
                                    <input type="checkbox" value="" className='form-check-input' />Etc
                                    </label>
                               </div> 
                           </div>  
						</div>						
                        
						<div className='row'>
                            <div className="col-xs-12 col-sm-12 col-lg-4 form-group">
                                <label>Location Plan Photo *</label>
                            </div>
                            <div className="col-xs-12 col-sm-12 col-lg-8 form-group">
                           
                            <div class="input-group mb-3">
							  <input type="text" placeholder="Select Image" class='form-control' />
                                <div class="input-group-append">
                                <span class="input-group-text">Upload</span>
                                </div>
                           </div>
                           </div>
                        </div>
						
						<div className='row'>
                            <div className="col-xs-12 col-sm-12 col-lg-4 form-group">
                                <label>Location Front View Photo *</label>
                            </div>
                            <div className="col-xs-12 col-sm-12 col-lg-8 form-group">
                         
                            <div class="input-group mb-3">
							 <input type="text" placeholder="Select Image" class='form-control' />
                                <div class="input-group-append">
                                <span class="input-group-text">Upload</span>
                                </div>
                               
                            </div>
                            </div>
                        </div>
						
						<div className='row'>
                            <div className="col-xs-12 col-sm-12 col-lg-4 form-group">
                                <label>Photo Right Side View Location *</label>
                            </div>
                            <div className="col-xs-12 col-sm-12 col-lg-8 form-group">
                          
                            <div class="input-group mb-3">
							 <input type="text" placeholder="Select Image" class='form-control' />
                                <div class="input-group-append">
                                <span class="input-group-text">Upload</span>
                                </div>
                               
                            </div>
                            </div>
                        </div>
						
						<div className='row'>
                            <div className="col-xs-12 col-sm-12 col-lg-4 form-group">
                                <label>Photo Left Side View Location *</label>
                            </div>
                            <div className="col-xs-12 col-sm-12 col-lg-8 form-group">
                          
                            <div class="input-group mb-3">
							 <input type="text" placeholder="Select Image" class='form-control' />
                                <div class="input-group-append">
                                <span class="input-group-text">Upload</span>
                                </div>
                               
                            </div>
                          </div>
                        </div>
						
						<div className='row'>
                            <div className="col-xs-12 col-sm-12 col-lg-4 form-group">
                                <label>Photo of the Opposite View of the Building *</label>
                            </div>
                            <div className="col-xs-12 col-sm-12 col-lg-8 form-group">
                        
                              <div class="input-group mb-3">
							    <input type="text" placeholder="Select Image" class='form-control' />
                                  <div class="input-group-append">
                                  <span class="input-group-text">Upload</span>
                                </div>
                            </div>
                           </div>
                        </div>
                                               

                        <NavLink to="/Login" className="tn">
                        <div className='row'>
                            <div className="col-xs-12 col-sm-12 col-lg-12 form-group dg">
                            <button type="button" className="btn btn-danger btn-sub">Submit</button>
                            </div>
                        </div>
                        </NavLink>

                    </form>


                </div>
            </div>

        </div>
    )
}

export default Yes;