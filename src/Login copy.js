import React, { useState } from 'react';
import { NavLink } from 'react-router-dom';
import { useNavigate } from "react-router-dom";
import axios from 'axios';

function Login() {
    const [email, setEmail] = useState();
    const [password, setPassword]= useState();
    const [output, setOutput] = useState("");
    let navigate = useNavigate();
    
    const login = () => {
        let path = '/Home';        

        if (!email && !password) {
            alert("Kindly enter email & password");            
        }
        else {
            
            const post=null;
            //alert('val here');
           
            const outputt=axios.post('https://api-repair.eratech.id/referraldashboard/fuser', { email: 'aatheshsoft@gmail.com',password : '123456'})
            .then(res => {
                console.log(res)
                setOutput(res.data)
            })
            .catch(function(response){
                alert("error"+response);
            });
                alert("out"+output);
                  const json = JSON.stringify(outputt);
                alert("after post");
                alert (" status "+json);
            if (!post) return null;
            
           // navigate(path);
        }
    }

    return (
        <div className='container'>
            <div className='row justify-content-center'>
                <div className="col-xs-12 col-sm-12 col-lg-5 form-group">
                    <form className="form-container">
                    <h5 className='text-center'>Enter Your Account</h5>
                    <hr/>
                        <div className='row'>
                            <div className="col-xs-12 col-sm-12 col-lg-12 form-group">
                                <label>Email:</label>
                                <input type="text" className="form-control" id="email" placeholder="Enter email" required onChange={(e)=>setEmail(e.target.value)}  />
                            </div>
                        </div>
                        <div className='row'>
                            <div className="col-xs-12 col-sm-12 col-lg-12 form-group">
                                <label>Password: </label>
                                <input type="password" className="form-control" id="password" placeholder="Enter password" required onChange={(e)=>setPassword(e.target.value)} />
                            </div>
                        </div>

                        <NavLink to="/Forgot">
                        <h6 className='text-right btn-clr form-group'>Forgot Password ?</h6>
                        </NavLink>
                        
                        <button type="submit" class="btn btn-danger btn-block"  onClick={login}>Enter</button>
                        
                    </form>
                </div>
            </div>
        </div >

    )
}
export default Login;   