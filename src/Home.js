import React, { useState,useEffect } from 'react';
import {NavLink} from 'react-router-dom';
import axios from 'axios';

function makeid(length) {
    var result           = '';
    var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
    var charactersLength = characters.length;
    for ( var i = 0; i < length; i++ ) {
      result += characters.charAt(Math.floor(Math.random() * 
 charactersLength));
   }
   return result;
}
function Home() {
    const email=window.sessionStorage.getItem("email");
    const referalcode =makeid(9);
    window.sessionStorage.setItem("referal_code", referalcode);
    axios.post('https://api-repair.eratech.id/referraldashboard/fuser/referal',{email:email,referal_code:referalcode})
    .then(function (response) {
        console.log(response)
    })
    .catch(function (error) {
        console.log(error)
    }) 
    return (
        <div className=''>
            <br />

            <div className=''>
             

                <div className='container text-center'>
                <img src={require('./Images/grey.jpg')} alt="Referral Program" />
                </div>

                <br />
                <div className='form-container br-clr pad'>
                    <h5 className='text-center'>Check My Referrals </h5>
                    <br />

                    <div className='row'>
                        <div className='col-xs-12 col-sm-12 col-lg-3 form-group'></div>
                        <div className='col-xs-12 col-sm-12 col-lg-3 form-group text-center'>
                            <NavLink to="/ReferralCode">
                                <button type="submit" class="btn btn-danger" >
                                    My Referral code
                                </button>
                            </NavLink>
                        </div>
                        <div className='col-xs-12 col-sm-12 col-lg-3 form-group text-center'>
                         <NavLink to="/Commission">
                            <button type="submit" class="btn btn-danger btn-rev" >
                                My Referral Status
                            </button>
                            </NavLink>
                        </div>
                    </div>
                </div>

            </div>

            <br /><br />
            <h6 className='text-center g-clr'>COPYRIGHT @ 2022 ERAJAYA GROUP ALL RIGHTS RESERVED</h6>
        </div>
    )
}

export default Home;