import QRCode from 'qrcode';
import { useEffect, useState } from 'react';
//import reportWebVitals from './reportWebVitals';

const ReferralQr = ({text}) => {
  const referal_code=window.sessionStorage.getItem("referal_code");
  const [src, setSrc] = useState("");

  useEffect(() => {
    QRCode.toDataURL(referal_code).then((setSrc));
   }, []);

return (
   <div>
    <img src={src} />
    </div>
    );
};
export default ReferralQr;
