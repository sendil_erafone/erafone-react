import React, { useState,useEffect } from 'react';
import { NavLink } from 'react-router-dom';
import { useNavigate } from 'react-router';
import { selectedIdsLookupSelector } from '@mui/x-data-grid';
import SelectInput from '@mui/material/Select/SelectInput';
import swal from 'sweetalert2';
//import swal from sweetalert;

async function loginUser(credentials) {
    return fetch('https://api-repair.eratech.id/referraldashboard/fuser', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(credentials)
    })
      .then(data => data.json())
   }

function Login() {
    const [email,setEmail]= useState("");
    const [password,setPassword]= useState("");
    const [output,setOutput] = useState("");

    let navigate=useNavigate();
    const handleSubmit = async e => {
        e.preventDefault();
        const response = await loginUser({
          email:email,
          password:password
        });
        if (response.status === 200) {
            window.sessionStorage.setItem("email", email);
            navigate('/Home');
          swal.update("Success", response.status, "200", {
            buttons: false,
            timer: 2000,
          })
          .then((value) => {
            localStorage.setItem('accessToken', response['status']);
            localStorage.setItem('user', JSON.stringify(response['data']));
          });
          alert('value here');
          
        } else {
          swal.fire("Failed", response.error_message, "error");
        }
      }
  
    return (
        <div className='container'>
            <div className='row justify-content-center'>
                <div className="col-xs-12 col-sm-12 col-lg-5 form-group">
                    <form className="form-container" onSubmit={handleSubmit}>
                        <h5 className='text-center'>Enter Your Account</h5>
                        <hr />
                        <div className='row'>
                            <div className="col-xs-12 col-sm-12 col-lg-12 form-group">
                                <label>Email:</label>
                                <input type="text" className="form-control" id="email" placeholder="Enter email" required  onChange={(e)=>setEmail(e.target.value)}/>
                            </div>
                        </div>
                        <div className='row'>
                            <div className="col-xs-12 col-sm-12 col-lg-12 form-group">
                                <label>Password: </label>
                                <input type="password" className="form-control" id="password" placeholder="Enter password" required onChange={(e)=>setPassword(e.target.value)}/>
                            </div>
                        </div>

                        <NavLink to="/Forgot">
                            <h6 className='text-right btn-clr form-group'>Forgot Password ?</h6>
                        </NavLink>

                        <button type="submit" class="btn btn-danger btn-block">Enter</button>

                    </form>
                </div>
            </div>
        </div >
    )
}


export default Login;   