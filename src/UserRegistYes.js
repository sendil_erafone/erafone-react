import React from 'react';
import { NavLink } from 'react-router-dom';


function UserRegistYes() {

    return (
        <div className='container'>
            <br />
            <div className="card">
                <div className="card-body">
                    <h3 className='text-center'>FILL IN THE FORM BELOW TO BECOME PART OF US</h3>
                    <br />
                    <h4 className='text-center'>APPLICANT'S PERSONAL DATA</h4>
                    <hr />

                    <form className='container'>
                    <div className='row'>
                            <div className="col-xs-12 col-sm-12 col-lg-4 form-group">
                                <label>Getting References *</label>
                            </div>
                            <div className="col-xs-12 col-sm-12 col-lg-8 form-group">
                            <div class="form-check-inline">
                               <label class="form-check-label">
                                   <input type="radio" name="man" class='form-check-input' />Yes
                               </label>
                            </div>
                            <div class="form-check-inline">
                            <NavLink to="/UserRegistNo">
                              <label class="form-check-label">
                                <input type="radio" name="woman" className='form-check-input' />No
                              </label>
                              </NavLink>
                            </div>
                            </div>
                        </div>
                        <div className='row'>
                            <div className="col-xs-12 col-sm-12 col-lg-4 form-group">
                                <label>Code Referell </label>
                            </div>
                            <div className="col-xs-12 col-sm-12 col-lg-8 form-group">
                            <div class="input-group">
                             <input type="text" placeholder="ABCD1234567890" class='form-control' />
                                <div class="input-group-append">
                                <span class="btn btn-success">cek kode</span>
                                </div>
                            </div>
                            </div>
                        </div>

                        <div className='row'>
                            <div className="col-xs-12 col-sm-12 col-lg-4 form-group">
                                <label>Full Name *</label>
                            </div>
                            <div className="col-xs-12 col-sm-12 col-lg-8 form-group">
                                <input type="text" placeholder='FullName' name="fullname" className='form-control' />
                            </div>
                        </div>

                        <div className='row'>
                            <div className="col-xs-6 col-sm-6 col-lg-4 form-group">
                                <label>Gender *</label>
                            </div>
                            <div className="col-xs-1 col-sm-1 col-lg-8 form-group">  
                            <div class="form-check-inline">
                               <label class="form-check-label">
                                   <input type="radio" name="man" class='form-check-input' />Man
                               </label>
                            </div>
                            <div class="form-check-inline">
                              <label class="form-check-label">
                                <input type="radio" name="woman" className='form-check-input' />Woman
                              </label>
                            </div>
                            </div>
                        </div>

                        <div className='row'>
                            <div className="col-xs-12 col-sm-12 col-lg-4 form-group">
                                <label>Province *</label>
                            </div>
                            <div className="col-xs-12 col-sm-12 col-lg-8 form-group">
                                <input type="text" placeholder='Province' name="province" className='form-control' />
                            </div>
                        </div>

                        <div className='row'>
                            <div className="col-xs-12 col-sm-12 col-lg-4 form-group">
                                <label>Municipality / District *</label>
                            </div>
                            <div className="col-xs-12 col-sm-12 col-lg-8 form-group">
                                <input type="text" placeholder='Municipality / District' name="municipal" className='form-control' />
                            </div>
                        </div>

                        <div className='row'>
                            <div className="col-xs-12 col-sm-12 col-lg-4 form-group">
                                <label>District *</label>
                            </div>
                            <div className="col-xs-12 col-sm-12 col-lg-8 form-group">
                                <input type="text" placeholder='District' name="district" className='form-control' />
                            </div>
                        </div>

                        <div className='row'>
                            <div className="col-xs-12 col-sm-12 col-lg-4 form-group">
                                <label>Village *</label>
                            </div>
                            <div className="col-xs-12 col-sm-12 col-lg-8 form-group">
                                <input type="text" placeholder='Village' name="Village" className='form-control' />
                            </div>
                        </div>

                        <div className='row'>
                            <div className="col-xs-12 col-sm-12 col-lg-4 form-group">
                                <label>Home Address *</label>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-lg-8 form-group">
                                  <textarea class="form-control" rows="5" id="comment"></textarea>
                            </div>
          
                        </div>

                        <div className='row'>
                            <div className="col-xs-12 col-sm-12 col-lg-4 form-group">
                                <label>RT / RW</label>
                            </div>
                            <div class="form-row col-xs-12 col-sm-12 col-lg-8 form-group">
									<div class="col">
									     <input type="text" placeholder='RT' name="rt" className='form-control' />
									</div>
									<div class="col">
									    <input type="text" placeholder='RW' name="rw" className='form-control' />
									</div>
								  </div>
                        </div>

                        <div className='row'>
                            <div className="col-xs-12 col-sm-12 col-lg-4 form-group">
                                <label>Postal Code *</label>
                            </div>
                            <div className="col-xs-12 col-sm-12 col-lg-8 form-group">
                                <input type="text" placeholder='Postal Code' name="postalcode" className='form-control' />
                            </div>
                        </div>

                        <div className='row'>
                            <div className="col-xs-12 col-sm-12 col-lg-4 form-group">
                                <label>Phone / Fax</label>
                            </div>
                            <div className="col-xs-12 col-sm-12 col-lg-8 form-group">
                                <input type="text" placeholder='Phone / Fax' name="phone" className='form-control' />
                            </div>
                        </div>

                        <div className='row'>
                            <div className="col-xs-12 col-sm-12 col-lg-4 form-group">
                                <label>Cellphone *</label>
                            </div>
                            <div className="col-xs-12 col-sm-12 col-lg-8 form-group">
                            <form>
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                <span class="input-group-text">62</span>
                                </div>
                                <input type="text" placeholder="XXXXXXXXX" class='form-control' />
                                <label>Pastikan bahwa nomor handphone yang Anda masukkan merupakan nomor handphone yang dapat dihubungi melalui WhatsApp</label>
                            </div>

                            
                            </form>
                            </div>
                        </div>


                        <div className='row'>
                            <div className="col-xs-12 col-sm-12 col-lg-4 form-group">
                                <label>Email *</label>
                            </div>
                            <div className="col-xs-12 col-sm-12 col-lg-8 form-group">
                                <input type="text" placeholder='Email' name="email" className='form-control' />
                            </div>
                        </div>
                                              
                        <NavLink to="/Login" className="tn">
                        <div className='row'>
                            <div className="col-xs-12 col-sm-12 col-lg-12 form-group dg">
                            <button type="button" className="btn btn-danger btn-sub">Submit</button>
                            </div>
                        </div>
                        </NavLink>

                    </form>

                </div>
            </div>

        </div>
    )
}

export default UserRegistYes;