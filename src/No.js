import React from 'react';
import { NavLink } from 'react-router-dom';


function No() {

    return (
        <div className='container'>
            <br />
            <div className="card">
                <div className="card-body">
                    <h3 className='text-center'>FILL IN THE FORM BELOW TO BECOME PART OF US</h3>
                    <br />
                    <h4 className='text-center'>PREFERRED STORE LOCATION</h4>
                    <hr />

                    <form className='container'>
                        <div className='row'>
                            <div className="col-xs-12 col-sm-12 col-lg-4 form-group">
                                <label>Province *</label>
                            </div>
                            <div className="col-xs-12 col-sm-12 col-lg-8 form-group">
                                <input type="text" placeholder='Province' name="province" className='form-control' />
                            </div>
                        </div>

                        <div className='row'>
                            <div className="col-xs-12 col-sm-12 col-lg-4 form-group">
                                <label>Municipality / District</label>
                            </div>
                            <div className="col-xs-12 col-sm-12 col-lg-8 form-group">
                                <input type="text" placeholder='Municipality / District' name="municipal" className='form-control' />
                            </div>
                        </div>
						
						 <div className='row'>
                            <div className="col-xs-6 col-sm-6 col-lg-4 form-group">
                                <label>Location Form *</label>
                            </div>
                            <div className="col-xs-1 col-sm-1 col-lg-8 form-group">  
                            <div class="form-check-inline">
                               <label class="form-check-label">
                                   <input type="radio" name="shop" class='form-check-input' />Shop
                               </label>
                            </div>
                            <div class="form-check-inline">
                              <label class="form-check-label">
                                <input type="radio" name="kiosk_tbusiness" className='form-check-input' />Kiosk/T.Business
                              </label>
                            </div>
                            </div>
                        </div>

                        <div className='row'>
                            <div className="col-xs-12 col-sm-12 col-lg-4 form-group">
                                <label>Rental Price / 5 Years *</label>
                            </div>
                            <div className="col-xs-12 col-sm-12 col-lg-8 form-group">
                                <input type="text" placeholder='Maximum Price' name="rental_price" className='form-control' />
                            </div>
                        </div>

                        <div className='row'>
                            <div className="col-xs-12 col-sm-12 col-lg-4 form-group">
                                <label>Village *</label>
                            </div>
                            <div className="col-xs-12 col-sm-12 col-lg-8 form-group">
                                <input type="text" placeholder='Village' name="Village" className='form-control' />
                            </div>
                        </div>

                        <div className='row'>
                            <div className="col-xs-12 col-sm-12 col-lg-4 form-group">
                                <label>Notes:</label>
                            </div>
						</div>
						
						<div className='row'>
                           <div className="col-xs-12 col-sm-12 col-lg-4 form-group">
                                <label>We will look for the best location that provides a return on investment of under 4 years</label>
                           </div>    
                        </div>

                      

                        <NavLink to="/Login" className="tn">
                        <div className='row'>
                            <div className="col-xs-12 col-sm-12 col-lg-12 form-group dg">
                            <button type="button" className="btn btn-danger btn-sub">Submit</button>
                            </div>
                        </div>
                        </NavLink>

                    </form>


                </div>
            </div>

        </div>
    )
}

export default No;