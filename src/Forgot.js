import React from 'react';
import { NavLink } from 'react-router-dom';

function Forgot() {

    return (
        <div className='container'>

            <div className='row justify-content-center'>
                
                <div className="col-xs-12 col-sm-12 col-lg-5 form-group">

                    <form className="form-container">

                        <h5 className='text-center'>FORGOT THE PASSWORD</h5>
                        <hr />
                        <h6 className='text-left'>Enter your email address for a link to change your password.</h6>
                        <br />
                        <div className='row'>
                            <div className="col-xs-12 col-sm-12 col-lg-12 form-group">
                                <label>Email *</label>
                                <input type="text" className="form-control" id="email" placeholder="Enter email" required />
                            </div>
                        </div>

                        <NavLink to="/">
                            <button type="submit" class="btn btn-danger btn-block">Submit</button>
                        </NavLink>

                    </form>
                </div>
            </div>
        </div >
    )
}

export default Forgot;